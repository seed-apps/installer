source "$(dirname $0)/settings.sh"

echo $WORKSPACE > "$WORKSPACE/path.string"
DATABASE_SETUP "apps.csv" "name;path;url;"


#--------------------------------------------
function NODE(){
#--------------------------------------------

    if [ ! -d "$WORKSPACE/$1" ]
    then
        echo "O $WORKSPACE/$1"
        git clone "$GIT_REPO/$1" "$WORKSPACE/$1"
        DATABASE_ADD "$1;$WORKSPACE/$1;$GIT_REPO/$1;"
    else
        echo "X $WORKSPACE/$1"
    fi
}
#--------------------------------------------
function ENV(){
#--------------------------------------------
    NODE "env/$1"
}
#--------------------------------------------
function LANGUAGE(){
#--------------------------------------------

    NODE "languages/$1"
}
#--------------------------------------------
function SERVICE(){
#--------------------------------------------
    NODE "language/$1"
}
#--------------------------------------------


case $1 in

    base)
        NODE "main"
        LANGUAGE "bash"
    ;;

    dev)
        NODE "main"
        NODE "dev"
        NODE "doc"
        NODE "test"
        LANGUAGE "bash"
        LANGUAGE "python"
        LANGUAGE "php"
        LANGUAGE "js"
        LANGUAGE "arduino"
    ;;

    *)
        SCRIPT="$1"
        if [ -f "$SCRIPT" ]
        then
            source "$SCRIPT"
        fi
    ;;

esac


